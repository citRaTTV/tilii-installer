if ($PSScriptRoot -eq "") {
    $PSScriptRoot = ([Environment]::GetCommandLineArgs()[0] -replace "Install TILII.exe", "")
}
$depsDir = "{0}\deps" -f $PSScriptRoot
$fivemDir = "{0}\TILII" -f $env:LOCALAPPDATA

Write-Host "Click OK to install Teamspeak. Make sure you close it when the install is complete."
Start-Process -Wait -FilePath ('{0}\teamspeak.exe' -f $depsDir) -PassThru *>$null
Start-Process -Wait -FilePath ('{0}\tokovoip.ts3_plugin' -f $depsDir) -PassThru *>$null

Write-Host "Click OK to install TILII FiveM. Make sure you close it when the install is complete.";
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut(("{0}\TILII.lnk" -f $WshShell.SpecialFolders("Desktop")))
$Shortcut.TargetPath = ("{0}\FiveM.exe" -f $fivemDir)
$Shortcut.IconLocation = ("{0}\TILII.ico" -f $fivemDir)
$Shortcut.Arguments = "+connect gta.tilii.tv:32066"
$Shortcut.Save() *>$null
if (-Not (Test-Path -Path $fivemDir)) {
	New-Item -ItemType Directory -Force -Path $fivemDir *>$null
	Copy-Item ('{0}\fivem.exe' -f $depsDir) -Destination ("{0}\FiveM.exe" -f $fivemDir) *>$null
	Copy-Item ('{0}\TILII.ico' -f $PSScriptRoot) -Destination ("{0}\TILII.ico" -f $fivemDir) *>$null
	Start-Process -WorkingDirectory $fivemDir -Wait -FilePath ("{0}\FiveM.exe" -f $fivemDir) -PassThru *>$null
}

Write-Host 'TILII shortcut has been placed on your Desktop. Install complete. Welcome to TILII!'